import React from "react";
import Form from "../components/form";
import Status from "../components/status";
import Images from "../components/images";

export default class App extends React.Component {

  componentDidMount() {
    this.props.fetchImages("Mountains");
  } 

  render() {
    return (
      <div className="App">
        <Form something={true} />
        <Status />
        <Images />
      </div>
    );
  }
}
