import React from 'react';
import Images from './Images';
import { GalleryContext } from '../../contexts/GalleryContext';

export default props => (<GalleryContext.Consumer>
    {({term, status, images}) => <Images {...props} term={term} status={status} images={images}/>}
</GalleryContext.Consumer>);