import React from 'react';
import Image from './Image';

export default ({images, term}) => (
    <div className="images-container">
    {images.map(image => <Image image={image} key={image.id} term={term} />)}
  </div>
);